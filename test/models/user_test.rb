require 'test_helper'

class UserTest < ActiveSupport::TestCase
  
  def setup
    @user = User.new(name: "Example User", email: "user@example.com",
                     password: "foobar", password_confirmation: "foobar")
  end
  
  test "should be valid" do
    assert @user.valid?
  end
  
  test "name should be present" do
    @user.name = ""
    assert_not @user.valid?
  end
  
  test "email should be present" do
    @user.email = ""
    assert_not @user.valid?
  end
  
  test "name should not be too long" do
    @user.name = "a" * 51
    assert_not @user.valid?
  end
  
  test "email should not be too long" do
    @user.email = "a" * 244 + "@example.com"
    assert_not @user.valid?
  end
  
  test "email validation should accept" do
    valid_emails = %w[user@example.com USER@foo.Com A_Us-ER@foo.bar.com
                     first.last@foo.jp alice+blob@baz.cn]
    valid_emails.each do |valid_email|
      @user.email = valid_email
      assert @user.valid?, "#{valid_email.inspect} should be valid"
    end
  end
  
  test "email validation should reject invalid address" do
    invalid_emails = %w[user@example,com User_.foo.org user.name@xamp.
                        foo@bar_za.com foo@bar+foo.cn.cn]
    invalid_emails.each do |invalid_email|
      @user.email = invalid_email
      assert_not @user.valid?, "#{invalid_email.inspect} should be invalid"
    end
  end
  
  test "email address should be unique" do
    duplicate_user = @user.dup
    duplicate_user.email = @user.email.upcase
    @user.save
    assert_not duplicate_user.valid?
  end
  
  test "email address should be saved as lower-case" do
    mixed_case_email = "Foo@ExAMple.Com"
    @user.email = mixed_case_email
    @user.save
    assert_equal mixed_case_email.downcase, @user.reload.email
  end
  
  test "password should be present" do
    @user.password = @user.password_confirmation = "" * 6
    assert_not @user.valid?
  end
  
  test "password should have a minimum length" do
    @user.password = @user.password_confirmation = "a" * 5
    assert_not @user.valid?
  end
  
  test "authenticated? should return false for a user with nil digest" do
    assert_not @user.authenticated?(:remember, '')
  end
  
  test "associated news should be destroyed" do
    @user.save
    @user.newsposts.create!(content: "Duh", title: "title")
    assert_difference 'Newspost.count', -1 do
      @user.destroy
    end
  end
  
  test "associated messages should be destroyed" do
    @user.save
    @user.messages.create!(content: "Woopy")
    assert_difference 'Message.count', -1 do
      @user.destroy
    end
  end
  
end
