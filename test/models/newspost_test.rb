require 'test_helper'

class NewspostTest < ActiveSupport::TestCase
  def setup 
    @user = users(:leeouch)
    @news = @user.newsposts.build(content: "Duh!", title: "Title")
  end
  
  test "content should be valid" do
    assert @news.valid?
  end
  
  test "user id should be present" do
    @news.user_id = nil
    assert_not @news.valid?
  end
  
  test "content should be present" do
    @news.content = ""
    assert_not @news.valid?
  end
  
  test "title should be valid" do
    @news.title = ""
    assert_not @news.valid?
  end
  
  test "content should be at most 400 characters" do
    @news.content = "a" * 401
    assert_not @news.valid?
  end
  
  test "title should be at most 36 characters" do
    @news.title = "a" * 37
    assert_not @news.valid?
  end
  
  test "order should be most recent first" do
    assert_equal newsposts(:most_recent), Newspost.first
  end
end
