require 'test_helper'

class AreaTest < ActiveSupport::TestCase
   
   def setup
     @area = Area.new(name: "Area1", coords: "11,11,22,22,33,33,44,44",
                      box_location: "left", description: "Testing",
                      creator: "Seita", diff_from: "Easy", diff_to: "Hard")
  end

  test "should be valid" do
    assert @area.valid?
  end  
  
  test "name should be present" do
    @area.name = " "
    assert_not @area.valid?
  end
  
  test "coords should be present" do
    @area.coords = " "
    assert_not @area.valid?
  end
  
  test "box_location shold be present" do
    @area.box_location = " "
    assert_not @area.valid?
  end
  
  test "creator should be present" do
    @area.creator = " "
    assert_not @area.valid?
  end
  
  test "diff_from should be present" do
    @area.diff_from = " "
    assert_not @area.valid?
  end
  
  test "diff_to can be blank" do
    @area.diff_to = " "
    assert @area.valid?
  end
  
  test "name should be 25 characters or less" do
    @area.name = "a" * 26
    assert_not @area.valid?
  end
  
  test "description should be 193 characters or less" do
    @area.description = "a" * 201
    assert_not @area.valid?
  end
  
  test "creator should be 50 characters or less" do
    @area.creator = "a" * 51
    assert_not @area.valid?
  end
  
  test "coords should be in pairs" do
    @area.coords = "11,22,11,22,11"
    assert_not @area.valid?
  end
  
  test "coords should be at less 3 pairs" do
    @area.coords = "11,22,11,22"
    assert_not @area.valid?
  end
end
