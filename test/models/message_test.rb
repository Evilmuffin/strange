require 'test_helper'

class MessageTest < ActiveSupport::TestCase
  
  def setup
    @user = users(:leeouch)
    @message = @user.messages.build(content: "Blha blah blah")
  end
  
  test "should be valid" do
    assert @message.valid?
  end
  
  test "user id should be present" do
    @message.user_id = nil
    assert_not @message.valid?
  end
  
  test "content should be present" do
    @message.content = " "
    assert_not @message.valid?
  end
  
  test "content should be at most 150 characters" do
    @message.content = "a" * 151
    assert_not @message.valid?
  end
  
  test "order should be most recent first" do
    assert_equal messages(:most_recent), Message.first
  end
    
end
