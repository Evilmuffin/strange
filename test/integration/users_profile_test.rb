require 'test_helper'

class UsersProfileTest < ActionDispatch::IntegrationTest
  
  def setup
    @user = users(:leeouch)
  end
  
  test 'profile disply' do
    get user_path(@user)
    assert_template 'users/show'
    assert_match @user.messages.count.to_s, response.body
  end
end
