require 'test_helper'

class MessagesInterfaceTest < ActionDispatch::IntegrationTest
  
  def setup
    @user = users(:leeouch)
  end
  
  test "message interface" do
    log_in_as(@user)
    assert_no_difference 'Message.count' do
      post messages_path, message: { content: "" }
    end
    assert_select 'div#error_explanation'
    # Valid submission
    content = "This is a test"
    assert_difference 'Message.count', 1 do
      post messages_path, message: { content: content }
    end
  end
end
