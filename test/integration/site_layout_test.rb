require 'test_helper'

class SiteLayoutTest < ActionDispatch::IntegrationTest
  
  def setup
    @admin = users(:leeouch)
    @non_admin = users(:arti)
  end
  
  test "layout links" do
    get root_path
    assert_template 'static_pages/home'
    assert_select "a[href=?]", root_path, count: 2
    assert_select "a[href=?]", messages_path
    assert_select "a[href=?]", building_path
    assert_select "a[href=?]", help_path
    assert_select "a[href=?]", about_path
    assert_select "a[href=?]", contact_path
    assert_select "a[href=?]", news_path
    assert_select "a[href=?]", login_path
    log_in_as(@non_admin)
    get root_path
    assert_select "a[href=?]", users_path, count: 1
    log_in_as(@admin)
    get root_path
    assert_select "a[href=?]", users_path
    assert_select "a[href=?]", edit_user_path(@admin)
    assert_select "a[href=?]", user_path(@admin)
    assert_select "a[href=?]", logout_path
    assert_select "a[href=?]", login_path, count: 0
  end

end