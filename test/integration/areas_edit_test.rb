require 'test_helper'

class AreasEditTest < ActionDispatch::IntegrationTest
  
  def setup
    @area = areas(:one)
    @user = users(:leeouch)
  end
  
  test "unsuccessful edit" do
    log_in_as(@user)
    get edit_area_path(@area)
    assert_template 'areas/edit'
    patch area_path(@area), area: { name: "",
                                    coords: "11,11,11",
                                    box_location: "",
                                    description: "",
                                    creator: "",
                                    diff_from: "",
                                    diff_to: "" }
    assert_template 'areas/edit'
  end
  
  test "successful edit" do
    log_in_as(@user)
    get edit_area_path(@area)
    assert_template 'areas/edit'
    patch area_path(@area), area: { name: "Oneone",
                                    coords: "11,11,11,11,11,11",
                                    box_location: "right",
                                    description: "changed",
                                    creator: "Seita",
                                    diff_from: "Easy",
                                    diff_to: "" }
    assert_not flash.empty?
    assert_redirected_to worldmap_path
    @area.reload
    assert_equal "Oneone", @area.name
    assert_equal "11,11,11,11,11,11", @area.coords
    assert_equal "right", @area.box_location
    assert_equal "changed", @area.description
    assert_equal "Seita", @area.creator
    assert_equal "Easy", @area.diff_from
    assert_equal "", @area.diff_to
  end
end
