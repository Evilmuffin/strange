require 'test_helper'

class MessagesEdithTest < ActionDispatch::IntegrationTest
  
  def setup
    @user = users(:leeouch)
    @message = messages(:orange)
  end
  
  test "unsuccessful edit" do
    log_in_as(@user)
    get edit_message_path(@message)
    assert_template 'messages/edit'
    patch message_path(@message), message: { content: "" }
    assert_template 'messages/edit'
  end
  
  test "successful edit" do
    log_in_as(@user)
    content = "Testing"
    get edit_message_path(@message)
    assert_template 'messages/edit'
    patch message_path(@message), message: { content: content }
    assert_not flash.empty?
    assert_redirected_to @user
    follow_redirect!
    assert_match content, response.body
  end
  
  test "should redirect edit when not logged in" do
    get edit_message_path(@message)
    assert_redirected_to login_url
  end
  
  test "should redirect edit when log in as wrong user" do
    log_in_as(users(:arti))
    get edit_message_path(@message)
    assert_redirected_to root_url
  end
end
