require 'test_helper'

class AddNewAreaTest < ActionDispatch::IntegrationTest
  
  def setup
    @user = users(:leeouch)
  end
  
  test "invalid new area info" do
    log_in_as(@user)
    get new_area_path
    assert_no_difference 'Area.count' do
      post areas_path, area: { name: "",
                               coords: "",
                               box_location: "",
                               description: "",
                               creator: "",
                               diff_from: "",
                               diff_to: "" }
      end
    assert_template 'areas/new'
  end
  
  test "valid new area info" do
    log_in_as(@user)
    get new_area_path
    assert_difference 'Area.count', 1 do
      post_via_redirect areas_path, area: { name: "test area",
                                            coords: "11,11,22,22,33,33",
                                            box_location: "left",
                                            description: "Blah blah blah",
                                            creator: "Seita",
                                            diff_from: "Easy",
                                            diff_to: "Easy" }
      end
    assert_template 'areas/index'
  end
end
