require 'test_helper'

class NewspostsInterfaceTest < ActionDispatch::IntegrationTest
  def setup
    @admin = users(:leeouch)
    @non_admin = users(:arti)
  end
  
  test "news interface" do
  # Log in as admin
    log_in_as(@admin)
    get news_path
    assert_select 'div.pagination'
  # Invalid submission
    assert_no_difference 'Newspost.count' do
      post newsposts_path, newspost: { content: "", title: "" }
    end
  # Valid submission
    title = "Title"
    content = "Blahblahblah"
    assert_difference 'Newspost.count', 1 do
      post newsposts_path, newspost: { content: content, title: title }
    end
    assert_redirected_to news_url
    follow_redirect!
    assert_match content, response.body
  # Delete a post
    assert_select 'a', text: 'delete'
    first_post = Newspost.first
    assert_difference "Newspost.count", -1 do
      delete newspost_path(first_post)
    end
  # Log in as non_admin
    log_in_as(@non_admin)
    assert_select 'a', text: 'delete', count: 0
  end
end
