require 'test_helper'

class NewspostsEdithTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:leeouch)
    @message = newsposts(:orange)
  end
  
  test "unsuccessful edit" do
    log_in_as(@user)
    get edit_newspost_path(@message)
    assert_template 'newsposts/edit'
    patch newspost_path(@message), newspost: { content: "", title: "" }
    assert_template 'newsposts/edit'
  end
  
  test "successful edit" do
    log_in_as(@user)
    content = "Testing"
    title = "Title"
    get edit_newspost_path(@message)
    assert_template 'newsposts/edit'
    patch newspost_path(@message), newspost: { content: content, title: title }
    assert_not flash.empty?
    assert_redirected_to news_url
    follow_redirect!
    assert_match content, response.body
  end
  
  test "should redirect edit when not logged in" do
    get edit_newspost_path(@message)
    assert_redirected_to login_url
  end
  
  test "should redirect edit when log in as non_admin" do
    log_in_as(users(:arti))
    get edit_newspost_path(@message)
    assert_redirected_to root_url
  end
end
