require 'test_helper'

class NewspostsControllerTest < ActionController::TestCase
  
  def setup
    @news = newsposts(:orange)
  end
  
  test "should get news" do
    get :news_page
    assert_response :success
  end
  
  test "should redirect create when not logged in" do
    assert_no_difference 'Newspost.count' do
      post :create, newspost: { content: "Lorem ipsum" }
    end
    assert_redirected_to login_url
  end
  
  test "should redirect destroy when not logged in" do
    assert_no_difference 'Newspost.count' do
      delete :destroy, id: @news
    end
    assert_redirected_to login_url
  end
  
  test "should redirect destroy for non_admin" do
    log_in_as(users(:arti))
    news_feed = newsposts(:ants)
    assert_no_difference 'Newspost.count' do
      delete :destroy, id: news_feed
    end
    assert_redirected_to root_url
  end
end
