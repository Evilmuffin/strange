require 'test_helper'

class AreasControllerTest < ActionController::TestCase
  def setup
    @user = users(:leeouch)
  end

  test "should get new" do
    log_in_as(@user)
    get :new
    assert_response :success
  end

end
