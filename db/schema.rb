# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160523133717) do

  create_table "areas", force: :cascade do |t|
    t.string   "name"
    t.text     "coords"
    t.string   "box_location"
    t.text     "description"
    t.string   "creator"
    t.string   "diff_from"
    t.string   "diff_to"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "messages", force: :cascade do |t|
    t.text     "content"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "messages", ["user_id", "created_at"], name: "index_messages_on_user_id_and_created_at"
  add_index "messages", ["user_id"], name: "index_messages_on_user_id"

  create_table "newsposts", force: :cascade do |t|
    t.text     "content"
    t.text     "title"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "newsposts", ["user_id", "created_at"], name: "index_newsposts_on_user_id_and_created_at"
  add_index "newsposts", ["user_id"], name: "index_newsposts_on_user_id"

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.string   "password_digest"
    t.string   "remember_digest"
    t.boolean  "admin",             default: false
    t.string   "activation_digest"
    t.boolean  "activated",         default: false
    t.datetime "activated_at"
    t.string   "reset_digest"
    t.datetime "reset_sent_at"
    t.string   "char1"
    t.string   "char2"
    t.string   "char3"
    t.string   "char4"
    t.string   "char5"
    t.string   "char1_lvl"
    t.string   "char2_lvl"
    t.string   "char3_lvl"
    t.string   "char4_lvl"
    t.string   "char5_lvl"
    t.string   "char1_class"
    t.string   "char2_class"
    t.string   "char3_class"
    t.string   "char4_class"
    t.string   "char5_class"
    t.string   "picture"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true

end
