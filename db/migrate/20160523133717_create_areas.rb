class CreateAreas < ActiveRecord::Migration
  def change
    create_table :areas do |t|
      t.string :name
      t.text :coords
      t.string :box_location
      t.text :description
      t.string :creator
      t.string :diff_from
      t.string :diff_to

      t.timestamps null: false
    end
  end
end
