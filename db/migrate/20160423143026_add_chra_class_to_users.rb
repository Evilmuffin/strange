class AddChraClassToUsers < ActiveRecord::Migration
  def change
    add_column :users, :char1_class, :string
    add_column :users, :char2_class, :string
    add_column :users, :char3_class, :string
    add_column :users, :char4_class, :string
    add_column :users, :char5_class, :string
  end
end
