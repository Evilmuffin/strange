class AddChraLvlToUsers < ActiveRecord::Migration
  def change
    add_column :users, :char1_lvl, :string
    add_column :users, :char2_lvl, :string
    add_column :users, :char3_lvl, :string
    add_column :users, :char4_lvl, :string
    add_column :users, :char5_lvl, :string
  end
end
