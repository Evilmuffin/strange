# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


User.create!(name: "Seita",
             email: "nope_not_here@yahoo.com",
             password: "immort",
             password_confirmation: "immort",
             admin: true,
             activated: true,
             activated_at: Time.zone.now)
             
45.times do |n|
  name = Faker::Name.name
  email = "fake-#{n+1}@fake.org"
  password = "password"
  User.create!(name: name,
               email: email,
               password: password,
               password_confirmation: password,
               activated: true,
               activated_at: Time.zone.now)
end

users = User.order(:created_at).take(6)
50.times do
  content = Faker::Lorem.sentence(5)
  title = "title"
  users.each { |user| user.news.create!(content: content, news_title: title) }
  users.each { |user| user.messages.create!(content: content) }
end
