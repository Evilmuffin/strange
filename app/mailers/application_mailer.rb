class ApplicationMailer < ActionMailer::Base
  default from: "noreply@strangemud.net"
  layout 'mailer'
end
