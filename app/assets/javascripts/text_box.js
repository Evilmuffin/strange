/* global $ */

$(document).ready(function(){
  $("area").mouseover(function(){
    $("#div1").load($(this).data('path'));
    if ($(this).data('box') == "left") {
      $("#boxleft").fadeIn(0);
      document.getElementById("boxleft").innerHTML = $(this).data('name');
    }
    else if ($(this).data('box') == "right") {
      $("#boxright").fadeIn(0);
      document.getElementById("boxright").innerHTML = $(this).data('name');
    }
    else if ($(this).data('box') == "top") {
      $("#boxtop").fadeIn(0);
      document.getElementById("boxtop").innerHTML = "▲ " + $(this).data('name');
    }
    else if ($(this).data('box') == "bottom") {
      $("#boxbottom").fadeIn(200);
      document.getElementById("boxbottom").innerHTML = "▼ " + $(this).data('name');
    }
    var path = $(this).data('bottom');
    $("#boxbottom").click(function(){
      window.location = path;
    });
  });
  $("area").mouseout(function(){
    $("#boxleft").fadeOut(0);
    $("#boxright").fadeOut(0);
    $("#boxtop").fadeOut(0);
  });
  $("#boxbottom").mouseout(function(){
    $("#boxbottom").fadeOut(200);
  });
});