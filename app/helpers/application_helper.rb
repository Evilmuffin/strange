module ApplicationHelper
  
  # Returns the full title on a per-page basis
  def full_title(page_title)
    page_title + " | StrangeMUD&trade;".html_safe
  end
end
