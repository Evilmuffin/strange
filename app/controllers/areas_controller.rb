class AreasController < ApplicationController
  before_action :logged_in_user, only: [:new, :create, :edit, :update, :destroy]
  before_action :admin_user, only: [:new, :create, :edit, :update, :destroy]
  
  def show
    @area = Area.find(params[:id])
  end
  
  def new
    @area = Area.new
  end
  
  def create
    @area = Area.new(area_params)
    if @area.save
      flash[:success] = "Area successfully saved!"
      redirect_to worldmap_url
    else
      render 'new'
    end
  end
  
  def index
    @areas = Area.all
  end
  
  def showall
    @areas = Area.paginate(page: params[:page], per_page: 10)
  end
  
  def edit
    @area = Area.find(params[:id])
  end
  
  def update
    @area = Area.find(params[:id])
    if @area.update_attributes(area_params)
      flash[:success] = "Area updated"
      redirect_to worldmap_url
    else
      render 'edit'
    end
  end
  
  def destroy
    Area.find(params[:id]).destroy
    flash[:success] = "Area deleted"
    redirect_to worldmap_url
  end
  
  private
  
    def area_params
      params.require(:area).permit(:name, :coords, :box_location, :description,
                                   :creator, :diff_from, :diff_to)
    end
end
