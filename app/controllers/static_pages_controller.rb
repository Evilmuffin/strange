class StaticPagesController < ApplicationController
  def home
  end

  # def history
  # end

  def help
  end

  def contact
  end
  
  def about
  end
  
  def building
  end
  
  def testpage
  end
  
end
