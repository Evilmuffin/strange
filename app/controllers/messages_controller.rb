class MessagesController < ApplicationController
  before_action :logged_in_user, only: [:create, :update, :edit, :destroy]
  before_action :correct_user, only: [:update, :edit, :destroy]
  
  def messages_board
    @message = current_user.messages.build if logged_in?
    @message_feed = Message.paginate(page: params[:page], per_page: 10)
  end
  
  def create
    @message = current_user.messages.build(message_params)
    if @message.save
      flash[:success] = "Message posted"
      redirect_to messages_url
    else
      @message_feed = []
      render 'messages/messages_board'
    end
  end
  
  def edit
    @message = Message.find(params[:id])
  end
  
  def update
    @message = Message.find(params[:id])
    if @message.update_attributes(message_params)
      flash[:success] = "Message updated"
      redirect_to current_user
    else
      render 'edit'
    end
  end
  
  def destroy
    @message.destroy
    flash[:success] = "Post deleted"
    redirect_to current_user
  end
  
  private
  
    def message_params
      params.require(:message).permit(:content)
    end
    
    def correct_user
      @message = current_user.messages.find_by(id: params[:id])
      redirect_to root_url if @message.nil?
    end
end
