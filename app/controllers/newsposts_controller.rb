class NewspostsController < ApplicationController
  
  before_action :logged_in_user, only: [:create, :edit, :update, :destroy]
  before_action :admin_user, only: [:create, :edit, :update, :destroy]
  before_action :admin_delete, only: :destroy
  
  def news_page
    @news = Newspost.paginate(page: params[:page], per_page: 10)
    if logged_in?
      @news_post = current_user.newsposts.build if current_user.admin?
      @news_feed = Newspost.paginate(page: params[:page], per_page: 10)
    end
  end
  
  def create
    @news_post = current_user.newsposts.build(newspost_params)
    if @news_post.save
      flash[:success] = "News updated!"
      redirect_to news_url
    else
      @news_feed = []
      render "newsposts/news_page"
    end
  end
  
  def edit
    @news_post = Newspost.find(params[:id])
  end
  
  def update
    @news_post = Newspost.find(params[:id])
    if @news_post.update_attributes(newspost_params)
      flash[:success] = "News item updated"
      redirect_to news_url
    else
      render 'edit'
    end
  end
  
  def destroy
    @news_delete.destroy
    flash[:success] = "News item deleted"
    redirect_to news_url
  end
  
  private
  
    def newspost_params
      params.require(:newspost).permit(:content, :title)
    end
    
    def admin_delete
      @news_delete = Newspost.find_by(id: params[:id])
      redirect_to root_url if @news_delete.nil?
    end
end
