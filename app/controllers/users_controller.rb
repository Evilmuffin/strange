class UsersController < ApplicationController
  before_action :logged_in_user, only: [:index, :edit, :update, :destroy, 
                                        :delete_inactive_users]
  before_action :correct_user, only: [:edit, :update]
  before_action :admin_user, only: [:destroy, :delete_inactive_users]
  
  def index
    if current_user.admin?
      @users = User.paginate(page: params[:page], per_page: 10)
    else
      @users = User.where(activated: true).paginate(page: params[:page],
                                                          per_page: 10)
    end
  end
  
  def delete_inactive_users
    @users = User.all
  end
  
  def show
    @user = User.find(params[:id])
    @messages = @user.messages.paginate(page: params[:page], per_page: 8)
    if !logged_in?
      redirect_to root_url and return unless @user.activated?
    elsif
      !current_user.admin?
      redirect_to root_url and return unless @user.activated?
    end
  end
  
  def new
    @user = User.new
  end
  
  def create
    @user = User.new(user_params)
    if @user.save
      @user.send_activation_email
      flash[:success] = 
      "Welcome to StrangeMUD! Please check your email to activate your account."
      redirect_to root_url
    else
      render 'new'
    end
  end
  
  def edit
    @user = User.find(params[:id])
  end
  
  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      flash[:success] = "Profile updated"
      redirect_to @user
    else
      render 'edit'
    end
  end
  
  def destroy
    User.find(params[:id]).destroy
    flash[:success] = "Delete succssful"
    redirect_to users_url
  end
  
  
  private
  
    def user_params
      params.require(:user).permit(:name, :email, :password,
                                   :password_confirmation, :char1, :char2,
                                   :char3, :char4, :char5, :char1_lvl,
                                   :char2_lvl, :char3_lvl, :char4_lvl,
                                   :char5_lvl, :char1_class, :char2_class,
                                   :char3_class, :char4_class, :char5_class,
                                   :picture)
    end
    
    # Before filters
    
    # Confirms a logged-in user
    
    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless current_user?(@user)
    end
    
end
