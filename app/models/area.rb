class Area < ActiveRecord::Base
  default_scope -> { order(name: :asc) }
  validates(:name, presence: true, length: { maximum: 25 })
  validates(:coords, presence: true)
  validates(:box_location, presence: true)
  validates(:description, presence: true, length: { maximum: 200})
  validates(:creator, presence: true, length: { maximum: 50 })
  validates(:diff_from, presence: true)
  validate(:coords_pairs)
  validate(:coords_length)
  
  private
  
    #Validates if coords are in pairs
    def coords_pairs
      coords_length = coords.split(",").length
      if coords_length % 2 == 1
        errors.add(:coords, "should be in pairs")
      end
    end
    
    def coords_length
      coords_length = coords.split(",").length
      if coords_length < 6
        errors.add(:coords, "should be at less 3 pairs")
      end
    end
end
