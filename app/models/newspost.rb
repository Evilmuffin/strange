class Newspost < ActiveRecord::Base
  belongs_to :user
  before_save { title.upcase! }
  default_scope -> { order(created_at: :desc) }
  validates(:user_id, presence: true)
  validates(:title, presence: true, length: { maximum: 36 })
  validates(:content, presence: true, length: { maximum: 400 })
end
