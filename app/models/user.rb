class User < ActiveRecord::Base
  has_many :newsposts, dependent: :destroy
  has_many :messages, dependent: :destroy
  attr_accessor :remember_token, :activation_token, :reset_token
  default_scope -> { order(name: :asc) }
  mount_uploader :picture, PictureUploader
  before_save :downcase_email
  before_save :capitalize_name
  before_create :create_activation_digest
  validates(:name, presence: true, length: { maximum: 20})
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.+[a-z\d\-]+)*\.[a-z]+\z/i
  validates(:email, presence: true, length: { maximum: 255},
            format: { with: VALID_EMAIL_REGEX },
            uniqueness: { case_sensitive: false},)
  has_secure_password
  validates(:password, presence: true, length: { minimum: 6 }, allow_nil: true)
  validate(:picture_size)
  validates(:char1, length: { maximum: 15 }, presence: true, 
            :if => lambda { |r| 
              r.char1_lvl? || r.char1_class? || 
              r.char2? || r.char2_lvl? || r.char2_class? ||
              r.char3? || r.char3_lvl? || r.char3_class? ||
              r.char4? || r.char4_lvl? || r.char4_class? ||
              r.char5? || r.char5_lvl? || r.char5_class? 
            })
  validates(:char1_lvl, presence: true, allow_blank: false,
            :if => lambda { |r| 
                r.char1? || r.char1_class? ||
                r.char2? || r.char2_lvl? || r.char2_class? ||
                r.char3? || r.char3_lvl? || r.char3_class? ||
                r.char4? || r.char4_lvl? || r.char4_class? ||
                r.char5? || r.char5_lvl? || r.char5_class?  
            })
  validates(:char1_class, presence: true, allow_blank: false,
            :if => lambda { |r| 
              r.char1? || r.char1_lvl? ||
              r.char2? || r.char2_lvl? || r.char2_class? ||
              r.char3? || r.char3_lvl? || r.char3_class? ||
              r.char4? || r.char4_lvl? || r.char4_class? ||
              r.char5? || r.char5_lvl? || r.char5_class?  
            })
  validates(:char2, length: { maximum: 15 }, presence: true, 
            :if => lambda { |r|  
              r.char2_lvl? || r.char2_class? ||
              r.char3? || r.char3_lvl? || r.char3_class? ||
              r.char4? || r.char4_lvl? || r.char4_class? ||
              r.char5? || r.char5_lvl? || r.char5_class? 
            })
  validates(:char2_lvl, presence: true, allow_blank: false,
            :if => lambda { |r| 
                r.char2? || r.char2_class? ||
                r.char3? || r.char3_lvl? || r.char3_class? ||
                r.char4? || r.char4_lvl? || r.char4_class? ||
                r.char5? || r.char5_lvl? || r.char5_class?  
            })
  validates(:char2_class, presence: true, allow_blank: false,
            :if => lambda { |r| 
              r.char2? || r.char2_lvl? || 
              r.char3? || r.char3_lvl? || r.char3_class? ||
              r.char4? || r.char4_lvl? || r.char4_class? ||
              r.char5? || r.char5_lvl? || r.char5_class?  
            })
  validates(:char3, length: { maximum: 15 }, presence: true, 
            :if => lambda { |r|  
              r.char3_lvl? || r.char3_class? ||
              r.char4? || r.char4_lvl? || r.char4_class? ||
              r.char5? || r.char5_lvl? || r.char5_class? 
            })
  validates(:char3_lvl, presence: true, allow_blank: false,
            :if => lambda { |r| 
                r.char3? || r.char3_class? ||
                r.char4? || r.char4_lvl? || r.char4_class? ||
                r.char5? || r.char5_lvl? || r.char5_class?  
            })
  validates(:char3_class, presence: true, allow_blank: false,
            :if => lambda { |r| 
              r.char3? || r.char3_lvl? || 
              r.char4? || r.char4_lvl? || r.char4_class? ||
              r.char5? || r.char5_lvl? || r.char5_class?  
            })
  validates(:char4, length: { maximum: 15 }, presence: true, 
            :if => lambda { |r|  
              r.char4_lvl? || r.char4_class? ||
              r.char5? || r.char5_lvl? || r.char5_class? 
            })
  validates(:char4_lvl, presence: true, allow_blank: false,
            :if => lambda { |r| 
                r.char4? || r.char4_class? ||
                r.char5? || r.char5_lvl? || r.char5_class?  
            })
  validates(:char4_class, presence: true, allow_blank: false,
            :if => lambda { |r| 
              r.char4? || r.char4_lvl? || 
              r.char5? || r.char5_lvl? || r.char5_class?  
            })
  validates(:char5, length: { maximum: 15 }, presence: true, 
            :if => lambda { |r|  
              r.char5_lvl? || r.char5_class? 
            })
  validates(:char5_lvl, presence: true, allow_blank: false,
            :if => lambda { |r| 
              r.char5? || r.char5_class?  
            })
  validates(:char5_class, presence: true, allow_blank: false,
            :if => lambda { |r| 
              r.char5? || r.char5_lvl?
            })



  
  # Returns the has digest of the given string.
  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end
  
  def User.new_token
    SecureRandom.urlsafe_base64
  end
  
  def remember
    self.remember_token = User.new_token
    update_attribute(:remember_digest, User.digest(remember_token))
  end
  
  def authenticated?(attribute, token)
    digest = send("#{attribute}_digest")
    return false if digest.nil?
    BCrypt::Password.new(digest).is_password?(token)
  end
  
  def forget
    update_attribute(:remember_digest, nil)
  end
  
  def activate
    update_columns(activated: true, activated_at: Time.zone.now)
  end
  
  def send_activation_email
    UserMailer.account_activation(self).deliver_now
  end
  
  def create_reset_digest
    self.reset_token = User.new_token
    update_columns(reset_digest: User.digest(reset_token),
                   reset_sent_at: Time.zone.now)
  end
  
  def send_password_reset_email
    UserMailer.password_reset(self).deliver_now
  end
  
  def password_reset_expired?
    reset_sent_at < 2.hours.ago
  end
  
  def feed
    News.all
  end
  
  def message_feed
    Messages.all
  end
  
  private
    
    def downcase_email
      self.email = email.downcase
    end
    
    def capitalize_name
      cap_name = self.name
      cap_name = cap_name.split
      cap_name.each do |i|
        i.capitalize!
      end
      cap_name = cap_name.join(" ")
      self.name = cap_name
    end
    
    def create_activation_digest
      self.activation_token = User.new_token
      self.activation_digest = User.digest(activation_token)
    end
    
    def picture_size
      if picture.size > 20.kilobytes
        errors.add(:picture, "should be less than 20kb")
      end
    end
end
