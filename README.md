The site was written in Ruby and Rails.

The structure is losely based on Michael Hartl Ruby on Rails Tutorial.  It
contains a few ActiveModels for Users, News and a Message Board.

Sign up is done in Rails with account activation via email.  Admin privilege
can only be granted at the console.

Avatar images are uploaded to AWS.

